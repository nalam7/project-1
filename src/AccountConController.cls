public with sharing class AccountConController {
    public AccountConController() {

    }
    @AuraEnabled(cacheable=true)
    public static list<contact> getRelatedContacts2(string input)
    {
        string keyword=input+'%';
        list<contact> lstacc=[select id,firstname,lastname,phone, email from 
                                contact where lastname like: keyword];
        return lstacc;
    }
    @AuraEnabled(cacheable=true)
    public static contact getRelatedContacts3(string input,string input1)
    {
        //account lstacc=[select id,name,(select lastname from contacts) from account where name =: input];
        contact lstcon=[select id,lastname,accountid from contact where id=:input1];
        lstcon.accountid=input;
        update lstcon;
        return lstcon;
    }
}